var express = require('express');
const fs = require('fs');

var router = express.Router();

/* GET users listing. */
const {
    updateUser,
    deleteUser,
    saveUser,
    getAll,
    getOne
} = require("../services/user.service");
const {
    isAuthorized
} = require("../middlewares/auth.middleware");


router.get('/', function(req, res) {
    if (req.query && req.query.id) {
        getOne(req.query.id).then((result) => {
            res.send(result);
        }).catch((error) => {
            res.send(error);
        })
    } else if (Object.keys(req.query).length === 0) {
        getAll().then((result) => {
            res.send(result);
        })
    } else {
        res('Wrong Parameter');
    }
});

router.post('/', isAuthorized, function(req, res, next) {
    saveUser(req.body).then((result) => {
        res.send(`${result} is saved`);
    }).catch((error) => {
        res.status(400).send(error);
    })
});


router.delete('/', isAuthorized, function(req, res, next) {
    deleteUser(req.query.id).then((result) => {
        res.send(result);
    }).catch((error) => {

        res.status(400).send(error);
    })
});

router.put('/', isAuthorized, function(req, res, next) {

    updateUser(req.query.id, req.body).then((result) => {
        res.send(result);
    }).catch((error) => {
        res.status(400).send(error);
    })
});

module.exports = router;