const fs = require('fs');
const Promise = require('promise');

const makeCSV = (result) => {
    const lines = [result.fields.join(',')];
    Object.keys(result.data).forEach((datakey) => {
        lines.push(datakey + ',' + result.data[datakey].join(','));
    })
    return lines.join('\n');
}

const saveData = (data) => {
    return new Promise((res, rej) => {
        getAllData().then((result) => {
            const maxId = Math.max.apply(Math, Object.keys(result.data));
            const newData = dataToArray(data, result.fields);
            result.data[maxId + 1] = newData;
            fs.writeFile('data/data.csv', makeCSV(result), (err) => {
                if (err) throw err;
                console.log('saved!');
            });

            res(newData);
        })
    });
}

function myParse(data) {
    const array = data.toString().split("\n");
    const keys = array.shift().split(',');
    const localData = {};
    localData.data = {};
    array.forEach((line) => {
        const lineArr = line.split(',');
        localData.data[lineArr[0]] = lineArr;
        lineArr.splice(0, 1);
    });
    localData.fields = keys;
    return localData;
}

let data;

const getAllData = () => {
    return new Promise((res, rej) => {
        if (!data) {
            fs.readFile('data/data.csv', (err, data) => {
                if (err) throw err;
                const db = myParse(data);
                res(db);
            });
        } else {
            res(db);
        }
    });
}

const getOneData = (id) => {
    return new Promise((res, rej) => {
        getAllData().then((result) => {
            const user = result.data[id];
            if (user) {
                res({
                    user: user,
                    fields: result.fields
                });
            } else {
                rej();
            }
        })
    });
}

const deleteData = (id) => {
    return new Promise((res, rej) => {

        getAllData().then((result) => {

            if (result.data[id]) {
                const deleted = result.data[id];
                delete result.data[id];
                fs.writeFile('data/data.csv', makeCSV(result), (err) => {
                    if (err) throw err;
                    res({
                        user: deleted
                    });
                });


            } else {
                rej('User is not present');
            }
        })
    });
}

const dataToArray = (data, fields) => {
    const newData = [];
    if (Object.prototype.toString.call(data) !== '[object Array]') {
        fields.forEach((fieldName, index) => {
            if (index) {
                newData.push(data[fieldName] || '');
            }
        })
    } else {
        newData = data;
    }
    return newData;
}

const updateData = (id, data) => {
    return new Promise((res, rej) => {
        getAllData().then((result) => {
            if (result.data[id]) {
                const arrayData = dataToArray(data, result.fields)
                result.data[id] = arrayData;
                fs.writeFile('data/data.csv', makeCSV(result), (err) => {
                    if (err) throw err;
                    console.log('updated!');
                });

                res(arrayData);
            } else {
                reject('id not found');
            }
        })
    });
}

module.exports = {
    updateData,
    saveData,
    getAllData,
    getOneData,
    deleteData
};