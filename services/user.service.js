/* services/user.service.js */
const Promise = require('promise');
const {
    saveData,
    deleteData,
    getAllData,
    updateData,
    getOneData
} = require("../repositories/user.repository");

const saveUser = (user) => {
    return new Promise((res, rej) => {
        if (user) {
            saveData(user).then((result) => {
                res(`${result[0]}`);
            }).catch((error) => rej(error));
        }
    })
};

const parseOne = (keys, values) => {
    const output = {};
    keys.forEach((key, keyIndex) => {
        output[key] = values[keyIndex];
    })
    return output;
}

const parseAll = (result) => {
    const output = {};
    result.fields.shift();
    Object.keys(result.data).forEach((resultIndex) => {

        output[resultIndex] = parseOne(result.fields, result.data[resultIndex]);
    })
    return output;

}


const getAll = () => {
    return new Promise((res, rej) => {
        getAllData().then((result) => {
            output = parseAll(result);
            res(output);
        }).catch(() => {
            rej(`no any data`);
        })
    })
}

const getOne = (id) => {
    return new Promise((res, rej) => {
        if (!id || !/\d+/.test(id)) {
            rej('Not an ID');
        } else {
            getOneData(id).then((result) => {
                result.fields.shift();
                res(parseOne(result.fields, result.user));
            }).catch(() => {
                rej(`user with ${id} not found`);
            })
        }
    });
}

const deleteUser = (id) => {
    return new Promise((res, rej) => {
        if (!id || !/\d+/.test(id)) {
            rej('Not an ID');
        } else {
            deleteData(id).then((result) => {
                res(`${result[0]} ${result[1]} was deleted`);
            }).catch((error) => {
                rej(error);
            })
        }
    });
}

const updateUser = (id, user) => {
    return new Promise((res, rej) => {
        if (user) {
            updateData(id, user).then((result) => {
                res(`${result[0]} ${result[1]} was updated`);
            }).catch((error) => rej(error));
        } else {
            rej('not an Id');
        }
    })
};

module.exports = {
    deleteUser,
    saveUser,
    getAll,
    updateUser,
    getOne
};